# Simulation Files
This folder contains a MATLAB implementation of the electromagnetic analysis of a bifurcated dual mode feed, as seen in the following image:
<p align="center">
<img src=https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT2NImhMf7VBXYL_mpuOoAn_26kpc_0ayBRjvFrolqGoja6DY7s&usqp=CAU />
</p>

## Mode Matching
This file calculates the scattering matrix parameters of the bifurcation and radiation plane using the mode matching solutions 
of the field expressions in the 3 cylindrical regions. By combining these parameters, the reflection coefficient and the modal 
amplitude ratio are plotted. To run this file, 3 special functions that calculate the TE and TM cut-off wavenumbers of each
region are needed, which are included in the subfolder Bessel Essentials (all material is reproduced according to its respective license,
which is also included). More details on this solution (albeit in Greek text) can be found [here](https://ikee.lib.auth.gr/record/317836/files/Kyriakou_Thesis.pdf).

## Parabolic Dish Patterns
This function implements the calculation of the various efficiency
factors, related to the radiation from a secondary apperture plane that a
parabolic reflector surface creates. The feed of the parabolic dish is a
dual mode TE11-TM11 bifurcated cylindrical waveguide. The inputs are:

   a:     radius of the radiating apperture in m

   f0:    vector of the frequrency interval [0.9fc,1.1fc] in GHz

   ratio: vector of the ratio Ate/Atm of the amplitudes of the TE and TM mode,
   respectively, for each frequency of the [0.9fc,1.1fc] interval

   Ref0:  vector of the reflection coefficient of the incoming wave in the first
   geometric region, for each frequency of the [0.9fc,1.1fc] interval

The outputs of the function are:

   eta_tot: vector of the total efficiency of the parabolic dish radiation plane,
   for each frequency of the [0.9fc,1.1fc] interval

   e_pr:    2-dim matrix of the primary radiation pattern of the feed
   apperture, size=[length(phi), length(theta)]

   e_secE:    vector of the E-plane secondary radiation pattern of the feed
   apperture

   e_secH:    vector of the H-plane secondary radiation pattern of the feed
   apperture
## Probe
This function calculates the ideal height and position, relative to a
perfect electric conductor, of an infinitely thin probe representing the
internal conductor of a coaxial 50 Ohm waveguide set to be perfectly
matched at a central frequency to a bifurcated cylindrical waveguide
feed. The input arguments are:

   a:     radius of input (region 1) of waveguide
   f0:    vector of the frequrency interval [0.9fc,1.1fc] in GHz
 
The outputs of the function are:

   Refpr: vector of reflection coefficient of the probe matching, for each
   frequency of the [0.9fc,1.1fc] interval
   
   