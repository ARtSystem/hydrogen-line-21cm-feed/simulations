function [eta_tot,e_pr, e_secE, e_secH] = parabolicdishpatterns(b, f0, ratio, Ref0)
% This function implements the calculation of the various efficiency
% factors, related to the radiation from a secondary apperture plane that a
% parabolic reflector surface creates. The feed of the parabolic dish is a
% dual mode TE11-TM11 bifurcated cylindrical waveguide. The inputs are:

%   a:     radius of the radiating apperture in m

%   f0:    vector of the frequrency interval [0.9*fc,1.1*fc] in GHz

%   ratio: vector of the ratio Ate/Atm of the amplitudes of the TE and TM mode,
%   respectively, for each frequency of the [0.9*fc,1.1*fc] interval

%   Ref0:  vector of the reflection coefficient of the incoming wave in the first
%   geometric region, for each frequency of the [0.9*fc,1.1*fc] interval

% The outputs of the function are:

%   eta_tot: vector of the total efficiency of the parabolic dish radiation plane,
%   for each frequency of the [0.9*fc,1.1*fc] interval

%   e_pr:    2-dim matrix of the primary radiation pattern of the feed
%   apperture, size=[length(phi), length(theta)]

%   e_secE:    vector of the E-plane secondary radiation pattern of the feed
%   apperture

%   e_secH:    vector of the H-plane secondary radiation pattern of the feed
%   apperture

k0=20/3*pi*f0; % wavenumber vector at c=c0
Z0=377; % characteristic impedance of the vacuum
[k,~]=BessDerivZerosBisect2(1,1); 
k=1/b*k; % cutoff wavenumber for TE11 mode
l=1/b*besselzero(1,1); % cutoff wavenumber for TM11 mode

% bessel functions
J0=@(x) besselj(0,x); 
J1=@(x) besselj(1,x);
J2=@(x) besselj(2,x);

%definition of parabolic reflector properties

D=3; %diameter
foD=0.35; % focal ratio
f=foD*D; % focal distance
thetaS=atan(0.5/(foD-1/(16*foD))); % half-opening angle
a_app=2*f*tan(thetaS/2); % apperture plane radius

theta=0:pi/360:pi; % theta discretization
thetap=theta(1:ceil(thetaS/pi*360));
phi=0:pi/180:2*pi; % phi discretization
[Theta,Phi]=meshgrid(theta,phi);
e_pr=zeros(length(phi),length(theta)); % primary pattern
e_secH=zeros(1,length(theta)); % secondary H-plane pattern
e_secE=zeros(1,length(theta)); % secondary E-plane pattern 

eta_tot=zeros(1,length(f0)); % total efficiency
eta_x=zeros(1,length(f0)); % cross-polarization efficiency
eta_ph=zeros(1,length(f0)); % phase efficiency
eta_app=zeros(1,length(f0)); % apperture efficiency
eta_sp=zeros(1,length(f0)); % spillover efficiency
eta_ill=zeros(1,length(f0)); % illumination efficiency
ephi=zeros(length(f0),length(theta)); % frequency response of theta-dependent phi-field component
etheta=zeros(length(f0),length(theta)); % frequency response of theta-dependent theta-field component

for i=1:1:length(f0)
    RTE=sqrt(pi/2)*J1(k*b)*sqrt(k^2*b^2-1); % normalization constant of TE21 mode
    RTM=0.5*sqrt(pi/2)*(J0(l*b)-J2(l*b))*(l*b); % normalization constant of TM11 mode
    ZTE=(Z0*k0(i))/sqrt(k0(i)^2-k^2); % characteristic impedance of TE11 mode
    ZTM=(Z0*sqrt(k0(i)^2-l^2))/k0(i); % characteristic impedance of TM11 mode
    
    Atm=sqrt(ZTM)/RTM; % renormalization of TM mode
    Ate=ratio(i)/RTE*sqrt(ZTE); % renormalization TE mode

    % integral expressions derived from apperture primary far-field calculations 
    eTEfphi=@(theta) Ate*(integral(@(r) 0.5*k*J2(k*r).*J2(k0(i)*r.*sin(theta)).*r,0,b)+integral(@(r) 0.5*k*J0(k*r).*J0(k0(i)*r.*sin(theta)).*r,0,b)).*(sqrt(k0(i)^2-k^2)/k0(i)+cos(theta));
    eTEftheta=@(theta) Ate*(integral(@(r) 0.5*k*J0(k*r).*J0(k0(i)*r.*sin(theta)).*r,0,b)-integral(@(r) 0.5*k*J2(k*r).*J2(k0(i)*r.*sin(theta)).*r,0,b)).*(1+sqrt(k0(i)^2-k^2)/k0(i)*cos(theta));
    
    eTMfphi=@(theta) 0; %Atm*(integral(@(r) 0.5*l*J0(l*r).*J0(k0(i)*r.*sin(theta)).*r,0,b)-integral(@(r) 0.5*l*J2(l*r).*J2(k0(i)*r.*sin(theta)).*r,0,b)).*(k0(i)/sqrt(k0(i)^2-l^2)+cos(theta));
    eTMftheta=@(theta)  Atm*(integral(@(r) l*J0(l*r).*J0(k0(i)*r.*sin(theta)).*r,0,b)).*(1+k0(i)/sqrt(k0(i)^2-l^2)*cos(theta));%+integral(@(r) 0.5*l*J2(l*r).*J2(k0(i)*r.*sin(theta)).*r,0,b)).*(1+k0(i)/sqrt(k0(i)^2-l^2)*cos(theta));
    
    % adding up renormalized TE and TM expressions
    efphi=@(theta) eTEfphi(theta)+eTMfphi(theta);
    eftheta=@(theta) eTEftheta(theta)+eTMftheta(theta);
    
    % integral expressions derived from apperture secondary far-field calculations 
    I2=@(theta) -pi*integral(@(r) J2(k0(i)*r*sin(theta)).*(efphi(2*atan(0.5*r/f))-eftheta(2*atan(0.5*r/f))).*r./(4*f^2+r.^2),0,a_app);
    I0=@(theta) -pi*integral(@(r) J0(k0(i)*r*sin(theta)).*(efphi(2*atan(0.5*r/f))+eftheta(2*atan(0.5*r/f))).*r./(4*f^2+r.^2),0,a_app);
    
    % for the central frequency, calculate the secondary patterns
    if i==floor(length(f0)/2)+1
        for j=1:1:119
            i2=I2(theta(j));
            i0=I0(theta(j));
            e_secH(j)=abs(i2)^2+abs(i2+i0)^2*cos(theta(j))^2;
            e_secE(j)=abs(i2+i0)^2;
        end
    end

    % calculate the far-field components (used in spillover efficiency),
    % and the primary pattern for the central frequency
    for j=1:1:length(theta)
        ephi(i,j)=efphi(theta(j));
        etheta(i,j)=eftheta(theta(j));
        if i==floor(length(f0)/2)+1
            for jj=1:1:length(phi)
                e_pr(jj,j)=cos(Phi(jj,j))^2*abs(ephi(i,j))^2+sin(Phi(jj,j))^2*abs(etheta(i,j))^2;
            end
        end
    end

    % integral expressions of the various efficiency nominator and
    % denominator factors, as derived by Collin
    eta_ph_nom=abs(integral(@(theta) (efphi(theta)+eftheta(theta)).*tan(theta/2),0,thetaS))^2;
    eta_ph_denom=integral(@(theta) abs(efphi(theta)+eftheta(theta)).*tan(theta/2),0,thetaS)^2;
    eta_ph(i)=eta_ph_nom/eta_ph_denom;

    eta_x_nom=integral(@(theta) (2*abs(efphi(theta)+eftheta(theta)).^2+abs(efphi(theta)-eftheta(theta)).^2).*sin(theta),0,thetaS);%integral(@(theta) (abs(efphi(phi,theta).*cos(phi)+eftheta(phi,theta).*sin(phi)).^2).*sin(theta),0,thetaS/2);
    eta_x_denom=integral(@(theta) 4*(abs(efphi(theta)).^2+abs(eftheta(theta)).^2).*sin(theta),0,thetaS);%integral2(@(phip,thetap) (abs(efphip(phip,thetap)).^2+abs(efthetap(phip,thetap)).^2).*sin(thetap),0,2*pi,0,thetaS/2);
    eta_x(i)=eta_x_nom/eta_x_denom;
        
    eta_app_nom=16*f^2/a_app^2*eta_ph_nom;
    eta_app_denom=eta_x_denom;
    eta_app(i)=eta_app_nom/eta_app_denom;
    
    % by Collin's definition
    eta_ill(i)=eta_app(i)/(eta_x(i)*eta_ph(i));

    eta_sp_nom=trapz(thetap,(abs(etheta(i,1:length(thetap))).^2+abs(ephi(i,1:length(thetap))).^2).*sin(thetap));
    eta_sp_denom=trapz(theta(1:180),(abs(etheta(i,1:180)).^2+abs(ephi(i,1:180)).^2).*sin(theta(1:180)));
    eta_sp(i)=eta_sp_nom/eta_sp_denom;
    
    % feed efficiency (losses are due to reflection)
    eta_f=1-abs(Ref0(i))^2;
    eta_tot(i)=eta_app(i)*eta_f*eta_sp(i);
    
end

end

