% mode matching of bifurcated cylindrical waveguide, radiation from open end
% and efficiency calculation - part1

% constants
f0=0.9*1.420405:0.005*1.420405:1.1*1.420405; %11.8:0.01:12.2; % frequency range
scale=12/1.420405; % scale factor from reference dimensions at 12GHz
lambda0=3./(10*f0); % wavelengths
k0=2*pi./lambda0; % wavenumbers at c=c0
Z0=377; % characteristic impedance at c=c0

% dimensions of feed at 12GHz
a=0.013*scale;
b=0.016*scale;
S=0.0076*scale;
T=0.04372*scale;

% define cutoff wave numbers for each mode of each region

% bessel functions
J0=@(x) besselj(0,x);
J1=@(x) besselj(1,x);
J2=@(x) besselj(2,x);
J1p=@(x) 1/2*(J0(x)-J2(x));
Y0=@(x) bessely(0,x);
Y1=@(x) bessely(1,x);
Y2=@(x) bessely(2,x);

N1=30; N2=45; N3=15; % modes in each region, N1+N3=N2 is satisfied
N=N2;
l12=besselzero(1,N2); % TM1n root, circular region
[k12,~]=BessDerivZerosBisect2(1,1:N2); % TE1n root, circular region

l3=zeros(1,N3); k3=zeros(1,N3);
l3(1)=besscrosszero(1,b/a,1,'DD'); % TM1n root ring region
k3(1)=a*fzero(@(x) (J0(a*x)-J2(a*x))*(Y0(b*x)-Y2(b*x))-(Y0(a*x)-Y2(a*x))*(J0(b*x)-J2(b*x)),70); % TE1n root ring region, indication of root derived from wolfram
for i=2:1:N3
    [l3(i),~]=besscrosszero(1,b/a,i,'DD');
    [k3(i),~]=besscrosszero(1,b/a,i,'NN');
end

% cutoff wavenumbers
l=[1/a*[l12(1:N1) zeros(1,N-N1)]; 1/b*l12; [1/a*l3 zeros(1,N-N3)]];
k=[1/a*[k12(1:N1) zeros(1,N-N1)]; 1/b*k12; [1/a*k3 zeros(1,N-N3)]];

% symbolic amplirudes - region 1
ainc=sym('ainc',[1 N1]); % TE1n incoming
aref=sym('aref',[1 N1]); % TE1n reflected
Ainc=sym('Ainc',[1 N1]); % TM1n incoming 
Aref=sym('Aref',[1 N1]); % TM1n reflected

% region 2
binc=sym('binc',[1 N2]);
bref=sym('bref',[1 N2]);
Binc=sym('Binc',[1 N2]);
Bref=sym('Bref',[1 N2]);

% region 3
cinc=sym('cinc',[1 N3]);
cref=sym('cref',[1 N3]);
Cinc=sym('Cinc',[1 N3]);
Cref=sym('Cref',[1 N3]);

% skip to part 4 where Strun is imported
%% part 2 - calculate mode coupling integrals

fn=zeros(N1,N2); % TE1n of region 1-TE1m of region 2 inner product integral
alphan=zeros(N1,N2); % TE1n of region 1-TM1m of region 2 inner product integral
zetan=zeros(N1,N2); % TM1n of region 1-TE1m of region 2 inner product integral
Fn=zeros(N1,N2); % TM1n of region 1-TM1m of region 2 inner product integral
gn=zeros(N3,N2); % TE1n of region 3-TE1m of region 2 inner product integral
nun=zeros(N3,N2); % TE1n of region 3-TM1m of region 2 inner product integral
mun=zeros(N3,N2); % TM1n of region 3-TE1m of region 2 inner product integral
Gn=zeros(N3,N2); % TM1n of region 3-TM1m of region 2 inner product integral

eTEr=cell(3,N); % TE1n e-field rho-component
eTEphi=cell(3,N); % TE1n e-field phi-component
eTMr=cell(3,N); % TM1n e-field rho-component
eTMphi=cell(3,N); % TM1n e-field rho-component

% calculations according to the normalized mode expression of each region
for j=1:1:N
    for i=1:1:2
        eTEr{i,j}=@(r) J1(k(i,j)*r)./r;
        eTEphi{i,j}=@(r) 0.5*k(i,j)*(J0(k(i,j)*r)-J2(k(i,j)*r));
        eTMr{i,j}=@(r) 0.5*l(i,j)*(J0(l(i,j)*r)-J2(l(i,j)*r));
        eTMphi{i,j}=@(r) J1(l(i,j)*r)./r;
    end
    eTEr{3,j}=@(r) 0.5*(-(J0(k(3,j)*a)-J2(k(3,j)*a))*Y1(k(3,j)*r)+(Y0(k(3,j)*a)-Y2(k(3,j)*a))*J1(k(3,j)*r))./r;
    eTEphi{3,j}=@(r) 0.25*k(3,j)*(-(J0(k(3,j)*a)-J2(k(3,j)*a))*(Y0(k(3,j)*r)-Y2(k(3,j)*r))+(Y0(k(3,j)*a)-Y2(k(3,j)*a))*(J0(k(3,j)*r)-J2(k(3,j)*r)));
    eTMr{3,j}=@(r) 0.5*l(3,j)*(-J1(l(3,j)*a)*(Y0(l(3,j)*r)-Y2(l(3,j)*r))+Y1(l(3,j)*a)*(J0(l(3,j)*r)-J2(l(3,j)*r)));
    eTMphi{3,j}=@(r) (-J1(l(3,j)*a)*Y1(l(3,j)*r)+J1(l(3,j)*r)*Y1(l(3,j)*a))./r;
    
    for t=1:1:min(j,N1)
      fn(t,j)=pi*integral(@(r) (eTEr{1,t}(r).*(eTEr{2,j}(r))+eTEphi{1,t}(r).*(eTEphi{2,j}(r))).*r,0,a);
      alphan(t,j)=pi*integral(@(r) (eTEr{1,t}(r).*(eTMr{2,j}(r))+eTEphi{1,t}(r).*(eTMphi{2,j}(r))).*r,0,a);
      zetan(t,j)=pi*integral(@(r) (eTMr{1,t}(r).*(eTEr{2,j}(r))+eTMphi{1,t}(r).*(eTEphi{2,j}(r))).*r,0,a);
      Fn(t,j)=pi*integral(@(r) (eTMr{1,t}(r).*(eTMr{2,j}(r))+eTMphi{1,t}(r).*(eTMphi{2,j}(r))).*r,0,a);
      
      if j<N1+1
        fn(j,t)=pi*integral(@(r) (eTEr{1,j}(r).*(eTEr{2,t}(r))+eTEphi{1,j}(r).*(eTEphi{2,t}(r))).*r,0,a);
        alphan(j,t)=pi*integral(@(r) (eTEr{1,j}(r).*(eTMr{2,t}(r))+eTEphi{1,j}(r).*(eTMphi{2,t}(r))).*r,0,a);
        zetan(j,t)=pi*integral(@(r) (eTMr{1,j}(r).*(eTEr{2,t}(r))+eTMphi{1,j}(r).*(eTEphi{2,t}(r))).*r,0,a);
        Fn(j,t)=pi*integral(@(r) (eTMr{1,j}(r).*(eTMr{2,t}(r))+eTMphi{1,j}(r).*(eTMphi{2,t}(r))).*r,0,a);
      end
    end
    for t=1:1:min(j,N3)
      gn(t,j)=pi*integral(@(r) (eTEr{3,t}(r).*(eTEr{2,j}(r))+eTEphi{3,t}(r).*(eTEphi{2,j}(r))).*r,a,b);
      nun(t,j)=pi*integral(@(r) (eTEr{3,t}(r).*(eTMr{2,j}(r))+eTEphi{3,t}(r).*(eTMphi{2,j}(r))).*r,a,b); 
      mun(t,j)=pi*integral(@(r) (eTMr{3,t}(r).*(eTEr{2,j}(r))+eTMphi{3,t}(r).*(eTEphi{2,j}(r))).*r,a,b);
      Gn(t,j)=pi*integral(@(r) (eTMr{3,t}(r).*(eTMr{2,j}(r))+eTMphi{3,t}(r).*(eTMphi{2,j}(r))).*r,a,b);
      
      if j<N3+1
          gn(j,t)=pi*integral(@(r) (eTEr{3,j}(r).*(eTEr{2,t}(r))+eTEphi{3,j}(r).*(eTEphi{2,t}(r))).*r,a,b);
          nun(j,t)=pi*integral(@(r) (eTEr{3,j}(r).*(eTMr{2,t}(r))+eTEphi{3,j}(r).*(eTMphi{2,t}(r))).*r,a,b);
          mun(j,t)=pi*integral(@(r) (eTMr{3,j}(r).*(eTEr{2,t}(r))+eTMphi{3,j}(r).*(eTEphi{2,t}(r))).*r,a,b);
          Gn(j,t)=pi*integral(@(r) (eTMr{3,j}(r).*(eTMr{2,t}(r))+eTMphi{3,j}(r).*(eTMphi{2,t}(r))).*r,a,b);
      end
    end
end

%% part 3 - define phase velocities for the modes and normalization constants-solve the mode matching equations

Strun=zeros(length(f0),16); % truncated (to hold only propagating modes) generalized scattering matrix

for i=1:1:length(f0)
    beta=sqrt(k0(i)^2-k.^2); % propagation constant of TE11
    gamma=sqrt(k0(i)^2-l.^2); % propagation constant of TM11
    P=[1 -1i*ones(1,N1-1) 1i*ones(1,N1) 1 -1i*ones(1,N2-1) 1 1i*ones(1,N2-1) 1 -1i*ones(1,N3-1) 1i*ones(1,N3)]; % wave amplitude real or imaginary unit sign relating to propagating or evanescent modes repsectively

    RTE=zeros(3,N); % TE1n normalization constants
    RTE(1:2,:)=sqrt(pi/2)*J1(([a; b]*ones(1,N)).*k(1:2,:)).*sqrt((([a; b]*ones(1,N)).*k(1:2,:)).^2-1);%.*sqrt(abs(beta(1:2,:))/(Z0*k0)); % circular regions (1,2)
    RTE(3,:)=sqrt(pi/2)*sqrt(((k(3,:)*b).^2-1).*(2*(J0(k(3,:)*a)-J2(k(3,:)*a))./(pi*b*k(3,:).*(J0(k(3,:)*b)-J2(k(3,:)*b)))).^2-((k(3,:)*a).^2-1).*(2./(pi*a*k(3,:))).^2);%.*sqrt(abs(beta(3,:))/(Z0*k0)); ring region (3)

    RTM=zeros(3,N); % TM1n normalization constants
    RTM(1:2,:)=0.5*sqrt(pi/2)*(J0(([a; b]*ones(1,N)).*l(1:2,:))-J2(([a; b]*ones(1,N)).*l(1:2,:))).*(([a; b]*ones(1,N)).*l(1:2,:));%.*sqrt(k0./(Z0*abs(gamma(1:2,:)))); % circular regions (1,2)
    RTM(3,:)=sqrt(pi/2)*sqrt(((l(3,:)*b).^2).*(2*J1(l(3,:)*a)./(pi*b*l(3,:).*J1(l(3,:)*b))).^2-((l(3,:)*a).^2).*(2./(pi*a*l(3,:))).^2);%.*sqrt(k0./(Z0*abs(gamma(3,:)))); % ring region (3)

    ZTE=(Z0*k0(i))./abs(beta); % characteristic impedances of TE1n modes
    ZTM=(Z0*abs(gamma))/k0(i); % characteristic impedances of TM1n modes
    
    % renormalized inner-product mode integrals
    f=diag(1./RTE(1,1:N1))*fn*diag(1./RTE(2,1:N2));
    alpha=diag(1./RTE(1,1:N1))*alphan*diag(1./RTM(2,1:N2));
    zeta=diag(1./RTM(1,1:N1))*zetan*diag(1./RTE(2,1:N2));
    F=diag(1./RTM(1,1:N1))*Fn*diag(1./RTM(2,1:N2));
    g=diag(1./RTE(3,1:N3))*gn*diag(1./RTE(2,1:N2));
    nu=diag(1./RTE(3,1:N3))*nun*diag(1./RTM(2,1:N2));
    mu=diag(1./RTM(3,1:N3))*mun*diag(1./RTE(2,1:N2));
    G=diag(1./RTM(3,1:N3))*Gn*diag(1./RTM(2,1:N2));

    % define the equations satisfying the boundary conditions
    
    % EFBC projected on TE modes of region 2
    eqns11=(ainc+aref)*sqrt(diag(ZTE(1,1:N1)))*f*(sqrt(diag(1./ZTE(2,1:N2))))+(Ainc+Aref)*sqrt(diag(ZTM(1,1:N1)))*zeta*(sqrt(diag(1./ZTE(2,1:N2))))...
        +(cinc+cref)*sqrt(diag(ZTE(3,1:N3)))*g*(sqrt(diag(1./ZTE(2,1:N2))))+(Cinc+Cref)*sqrt(diag(ZTM(3,1:N3)))*mu*(sqrt(diag(1./ZTE(2,1:N2))))==(binc+bref)*eye(N2);
    
    % EFBC projected on TM modes of region 2
    eqns12=(ainc+aref)*sqrt(diag(ZTE(1,1:N1)))*alpha*(sqrt(diag(1./ZTM(2,1:N2))))+(Ainc+Aref)*sqrt(diag(ZTM(1,1:N1)))*F*(sqrt(diag(1./ZTM(2,1:N2))))...
        +(cinc+cref)*sqrt(diag(ZTE(3,1:N3)))*nu*(sqrt(diag(1./ZTM(2,1:N2))))+(Cinc+Cref)*sqrt(diag(ZTM(3,1:N3)))*G*(sqrt(diag(1./ZTM(2,1:N2))))==(Binc+Bref)*eye(N2);

    % MFBC projected on TE modes of region 2
    eqns13=(aref-ainc).*P(1:N1)*(sqrt(diag(1./ZTE(1,1:N1))))*f*sqrt(diag(ZTE(2,1:N2)))+(Aref-Ainc).*P(N1+1:2*N1)*(sqrt(diag(1./ZTM(1,1:N1))))*zeta*sqrt(diag(ZTE(2,1:N2)))...
        +(cref-cinc).*P(2*(N1+N2)+1:2*(N1+N2)+N3)*(sqrt(diag(1./ZTE(3,1:N3))))*g*sqrt(diag(ZTE(2,1:N2)))+(Cref-Cinc).*P(2*(N1+N2)+N3+1:4*N2)*(sqrt(diag(1./ZTM(3,1:N3))))*mu*sqrt(diag(ZTE(2,1:N2)))==(binc-bref).*P(2*N1+1:2*N1+N2)*eye(N2);
    
    % MFBC projected on TM modes of region 2
    eqns14=(aref-ainc).*P(1:N1)*(sqrt(diag(1./ZTE(1,1:N1))))*alpha*sqrt(diag(ZTM(2,1:N2)))+(Aref-Ainc).*P(N1+1:2*N1)*(sqrt(diag(1./ZTM(1,1:N1))))*F*sqrt(diag(ZTM(2,1:N2)))...
        +(cref-cinc).*P(2*(N1+N2)+1:2*(N1+N2)+N3)*(sqrt(diag(1./ZTE(3,1:N3))))*nu*sqrt(diag(ZTM(2,1:N2)))+(Cref-Cinc).*P(2*(N1+N2)+N3+1:4*N2)*(sqrt(diag(1./ZTM(3,1:N3))))*G*sqrt(diag(ZTM(2,1:N2)))==(Binc-Bref).*P(2*N1+N2+1:2*(N1+N2))*eye(N2);
    
    % EFBC projected on TE modes of region 1
    eqns21=(binc+bref)*sqrt(diag(ZTE(2,1:N2)))*f'*(sqrt(diag(1./ZTE(1,1:N1))))+(Binc+Bref)*sqrt(diag(ZTM(2,1:N2)))*alpha'*(sqrt(diag(1./ZTE(1,1:N1))))==(ainc+aref)*eye(N1);

    % EFBC projected on TM modes of region 1
    eqns22=(binc+bref)*sqrt(diag(ZTE(2,1:N2)))*zeta'*(sqrt(diag(1./ZTM(1,1:N1))))+(Binc+Bref)*sqrt(diag(ZTM(2,1:N2)))*F'*(sqrt(diag(1./ZTM(1,1:N1))))==(Ainc+Aref)*eye(N1);

    % EFBC projected on TE modes of region 3
    eqns23=(binc+bref)*sqrt(diag(ZTE(2,1:N2)))*g'*(sqrt(diag(1./ZTE(3,1:N3))))+(Binc+Bref)*sqrt(diag(ZTM(2,1:N2)))*nu'*(sqrt(diag(1./ZTE(3,1:N3))))==(cinc+cref)*eye(N3);
    
    % EFBC projected on TM modes of region 3
    eqns24=(binc+bref)*sqrt(diag(ZTE(2,1:N2)))*mu'*(sqrt(diag(1./ZTM(3,1:N3))))+(Binc+Bref)*sqrt(diag(ZTM(2,1:N2)))*G'*(sqrt(diag(1./ZTM(3,1:N3))))==(Cinc+Cref)*eye(N3);

    % MFBC projected on TE modes of region 1
    eqns25=(binc-bref).*P(2*N1+1:2*N1+N2)*(sqrt(diag(1./ZTE(2,1:N2))))*f'*sqrt(diag(ZTE(1,1:N1)))+(Binc-Bref).*P(2*N1+N2+1:2*(N1+N2))*(sqrt(diag(1./ZTM(2,1:N2))))*alpha'*sqrt(diag(ZTE(1,1:N1)))==(aref-ainc).*P(1:N1)*eye(N1);

    % MFBC projected on TM modes of region 1
    eqns26=(binc-bref).*P(2*N1+1:2*N1+N2)*(sqrt(diag(1./ZTE(2,1:N2))))*zeta'*sqrt(diag(ZTM(1,1:N1)))+(Binc-Bref).*P(2*N1+N2+1:2*(N1+N2))*(sqrt(diag(1./ZTM(2,1:N2))))*F'*sqrt(diag(ZTM(1,1:N1)))==(Aref-Ainc).*P(N1+1:2*N1)*eye(N1);

    % MFBC projected on TE modes of region 3
    eqns27=(binc-bref).*P(2*N1+1:2*N1+N2)*(sqrt(diag(1./ZTE(2,1:N2))))*g'*sqrt(diag(ZTE(3,1:N3)))+(Binc-Bref).*P(2*N1+N2+1:2*(N1+N2))*(sqrt(diag(1./ZTM(2,1:N2))))*nu'*sqrt(diag(ZTE(3,1:N3)))==(cref-cinc).*P(2*(N1+N2)+1:2*(N1+N2)+N3)*eye(N3);

    % MFBC projected on TM modes of region 3
    eqns28=(binc-bref).*P(2*N1+1:2*N1+N2)*(sqrt(diag(1./ZTE(2,1:N2))))*mu'*sqrt(diag(ZTM(3,1:N3)))+(Binc-Bref).*P(2*N1+N2+1:2*(N1+N2))*(sqrt(diag(1./ZTM(2,1:N2))))*G'*sqrt(diag(ZTM(3,1:N3)))==(Cref-Cinc).*P(2*(N1+N2)+N3+1:4*N2)*eye(N3);

    eqns=[eqns21 eqns23 eqns25 eqns27 eqns12 eqns14];

    % substitute unit forward wave amplitude of each propagating mode to find the truncated generalized scattering matrix

    inds=[1 2*N1+1 2*N1+N2+1 2*(N1+N2)+1]; % propagating mode indices
    for j=1:1:length(inds)
        curr_eqns=subs(eqns,[ainc Ainc binc Binc cinc Cinc],[zeros(1,inds(j)-1) 1 zeros(1,4*N2-inds(j))]); %subs([eqns1 eqns2 eqns3 eqns4 eqns5 eqns6 eqns7 eqns8],[ainc Ainc binc Binc cinc Cinc],[zeros(1,inds(j)-1) 1 zeros(1,size(Sc,1)-inds(j))]);
        [R,Q]=equationsToMatrix(curr_eqns,[aref Aref bref Bref cref Cref]);
        Sc=(linsolve(double(R),double(Q)));
        Strun(i,4*(j-1)+1:4*j)=[Sc(1) Sc(2*N1+1) Sc(2*N1+N2+1) Sc(2*(N1+N2)+1)];
    end
end

%% part 4 - calculate the radiation reflection of each mode and their self and cross coupling

% if you have matlab 2019a or earlier, skip to the next part
Srad=zeros(length(f0),4); % scattering matrix of radiated TE11, TM11 modes

Y1p=@(x) 0.5*(bessely(0,x,1)-bessely(2,x,1)); % bessel Y order 1 derivative
H2p=@(x) J1p(x)-1i*Y1p(x); % hankel type 2 order 1 derivative

I1p=@(x) 0.5*(besseli(0,x,1)+besseli(2,x,1)); % bessel I order 1 derivative
K1p=@(x) -0.5*(besselk(0,x,1)+besselk(2,x,1)); % bessel K order 1 derivative

% integral calculations of self- and cross-mode conductances
for i=1:1:length(f0)
    G11=integral(@(w) w.*sqrt(1-w.^2).*J1p(k0(i)*b*w).^2./(w.^2-(k(2,1)/k0(i))^2).^2,0,inf);
    F11e=integral(@(w) J1(k0(i)*b*w).^2./(sqrt(1-w.^2).*w),0,inf);
    yee=2*(k(2,1)*b)^4*k0(i)/(sqrt(k0(i)^2-k(2,1)^2)*((k(2,1)*b)^2-1))*(G11/(k0(i)*b)^2+1/(k(2,1)*b)^4*F11e); % TE11-TE11

    L11=integral(@(w) w.^3.*J1(k0(i)*b*w).^2./(sqrt(1-w.^2).*(w.^2-(l(2,1)/k0(i))^2).^2),0,inf);
    ymm=2*sqrt(k0(i)^2-l(2,1)^2)/k0(i)*L11; % TM11-TM11

    F11m=integral(@(w) w.*J1(k0(i)*b*w).^2./(sqrt(1-w.^2).*(w.^2-(l(2,1)/k0(i))^2)),0,inf);
    yem=2/sqrt((k(2,1)*b)^2-1)*sqrt(sqrt((k0(i)^2-l(2,1)^2)/(k0(i)^2-k(2,1)^2)))*F11m; % TE11-TM11

    y=[yee yem; yem ymm]; % conductance matrix
    Sr=inv(eye(2)+y)*(eye(2)-y); % transformation to scattering matrix
    Srad(i,:)=Sr(:).';
end

%% part 5 - find the total scattering matrix of the waveguide cascade

load('Strunnew.mat'); % comment this line if other central frequency has been selected
% load('Srad.m'); % comment this line and run part 4 if you have matlab 2019b or later
Ref0=zeros(1,length(f0)); % reflection coefficient of input port
ratio=zeros(1,length(f0)); % ratio of TE11 to TM11 forward wave amplitude

for i=1:1:length(f0)
    Iinc=sym('Iinc'); % TE11 incoming input (region 1)
    Iref=sym('Iref'); % TE11 reflected input (region 1)
    Oeinc=sym('Oeinc'); % TE11 incoming output (region 2)
    Oeref=sym('Oeref'); % TE11 reflected input (region 2)
    Ominc=sym('Ominc'); % TM11 incoming input (region 2)
    Omref=sym('Omref'); % TM11 reflected input (region 2)

    % waveguide region 1 propagation
    eqns1= Iref==exp(-1i*sqrt(k0(i)^2-k(1,1)^2)*S)'*aref(1);
    eqns2= ainc(1)==exp(-1i*sqrt(k0(i)^2-k(1,1)^2)*S)'*Iinc;

    % waveguide region 3 propagation
    eqns3= cinc(1)==-exp(-1i*2*sqrt(k0(i)^2-k(3,1)^2)*S)'*cref(1);

    % bifurcation plane (mode matching on boundary of regions 1,3-2)
    eqns4to7= [aref(1); bref(1); Bref(1); cref(1)]==reshape(Strun(i,:),[4 4])'*[ainc(1); binc(1); Binc(1); cinc(1)];

    % waveguide region 2 propagation
    eqns8= binc(1)==exp(-1i*sqrt(k0(i)^2-k(2,1)^2)*T)'*Oeinc;
    eqns9= Oeref==exp(-1i*sqrt(k0(i)^2-k(2,1)^2)*T)'*bref(1);
    eqns10= Binc(1)==exp(-1i*sqrt(k0(i)^2-l(2,1)^2)*T)'*Ominc;
    eqns11= Omref==exp(-1i*sqrt(k0(i)^2-l(2,1)^2)*T)'*Bref(1);

    % radiating apperture of region 2, mode self- and cross- coupling
    eqns12to13= [Oeinc; Ominc]==(reshape(Srad(i,:),[2 2]))*[Oeref; Omref]; 

    neweqns=[eqns1; eqns2; eqns3; eqns4to7; eqns8; eqns9; eqns10; eqns11; eqns12to13]; 

    % solve the linear system for unit forward wave amplitude of input
    curr_eqns=subs(neweqns,Iinc,1);%[Iinc Oeinc Ominc],[zeros(1,i-1) 1 zeros(1,3-i)]);
    [R,Q]=equationsToMatrix(curr_eqns,[Iref Oeinc Oeref Ominc Omref ainc(1) aref(1) binc(1) bref(1) Binc(1) Bref(1) cinc(1) cref(1)]);
    curr_sol=linsolve(R,Q);

    Ref0(i)=double(abs(curr_sol(1)));%double(subs(Ref,c,c0)));
    ratio(i)=double((curr_sol(3))/(curr_sol(5)));%double(subs((curr_sol(2)+curr_sol(3))/(curr_sol(4)+curr_sol(5)),c,c0));
end

%% part 6 - calculate the efficiencies and plots

Refpr = probe(a, f0);

plot(f0,abs(Refpr),'b'); 
grid on;
title('Reflection Coefficient of matching to a 50 \Omega coaxial');
xlabel('Frequency f(GHz)');
ylabel('\Gamma=(Z_{in}-50)/(Z_{in}+50)');

theta=0:pi/360:pi; % theta discretization
phi=0:pi/180:2*pi; % phi discretization
[Theta,Phi]=meshgrid(theta,phi);

% refcoeff=readmatrix('refcoeff.txt'); %comsol implementation 
plot(f0,mag2db(Ref0),'b'); % hold on; plot(refcoeff(:,1),mag2db(refcoeff(:,2)),'r');
grid on;
xlabel('Frequency f(GHz)');
ylabel('Reflection Coefficient S_{11}(dB)');
% legend('Analytic','FEM');

[eta_tot,e_pr, e_secE, e_secH] = parabolicdishpatterns(b, f0, ratio, Ref0);

% eff=readmatrix('efficiency.txt'); %comsol implementation
figure();
plot(f0,eta_tot,'b'); % hold on; plot(eff(:,1),eff(:,2)./(1-refcoeff(:,2)),'r');
grid on;
xlabel('Frequency f');
ylabel('total efficiency \eta_{tot}');
% legend('Analytic','FEM');

% directivity calculation for primary and secondary patterns
Ptot=trapz(theta,trapz(phi,e_pr.*sin(Theta),2)); % total power
Dir=4*pi*e_pr/Ptot;

PtotH=trapz(theta,e_secH.*sin(theta));
DirH=2*e_secH/PtotH;
PtotE=trapz(theta,e_secE.*sin(theta));
DirE=2*e_secE/PtotE;

% polar plots for primary and secondary patterns

%patternCustom(Dir,180*theta/pi,180*phi'/pi); % 3D pattern
figure();
set(gcf, 'Color', 'w');
polarpattern([180*theta/pi 180*theta/pi+180],10*log10([Dir(1,:) fliplr(Dir(181,:))]),'MagnitudeLim',[-20 12],'TitleTop','Directivity pattern (dB)','ColorOrderIndex',3);
hold on;
polarpattern([180*theta/pi 180*theta/pi+180],10*log10([Dir(91,:) fliplr(Dir(271,:))]),'ColorOrderIndex',4);
legend('H-plane','E-plane');

figure();
set(gcf, 'Color', 'w');
polarpattern([180*theta/pi 180*theta/pi+180],10*log10([DirH fliplr(DirH)]),'TitleTop','Directivity pattern (dB)','ColorOrderIndex',3);
hold on;
polarpattern([180*theta/pi 180*theta/pi+180],10*log10([DirE fliplr(DirE)]),'ColorOrderIndex',4);
legend('H-plane','E-plane');




