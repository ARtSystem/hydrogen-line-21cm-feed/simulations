function [Refpr] = probe(a, f0)
% This function calculates the ideal height and position, relative to a
% perfect electric conductor, of an infinitely thin probe representing the
% internal conductor of a coaxial 50 Ohm waveguide set to be perfectly
% matched at a central frequency to a bifurcated cylindrical waveguide
% feed. The input arguments are:
%
%   a:     radius of input (region 1) of waveguide
%   f0:    vector of the frequrency interval [0.9*fc,1.1*fc] in GHz
% 
% The outputs of the function are:

%   Refpr: vector of reflection coefficient of the probe matching, for each
%   frequency of the [0.9*fc,1.1*fc] interval
    
    % 1st method (Collin, 1960)
    
    i0=floor(length(f0)/2)+1; % central frequency index
    lambda0=3./(10*f0); % wavelengths
    k0=2*pi./lambda0; % wavenumbers at c=c0
    Z0=377; % characteristic impedance of the vacuum
    [k,~]=BessDerivZerosBisect2(1,1); k=1/a*k; % cutoff wavenumber for TE11 mode
    
    % bessel functions
    J0=@(x) besselj(0,x); 
    J1=@(x) besselj(1,x);

    mu0=1.256*10^(-6); % magnetic permittivity of vacuum
    ZTE=(Z0*k0(i0))/sqrt(k0(i0)^2-k^2); % characteristic impedance of TE11 mode
    RTE=sqrt(pi/2)*J1(k*a)*sqrt(k^2*a^2-1); % normalization constant of TE11 mode
    bTE=sqrt(k0(i0)^2-k^2); % propagation constant of TE11 mode
    PTE=2/(ZTE*RTE^2); % total constant
    I=@(h) integral(@(r) J1(k*r)./r.*sin(k0(i0)*(r-a+h)),a-h,a);

    lambdag=2*pi/bTE; % group wavelength 
    
    % real and imaginary parts of impedance as calculated by Collin's method
    Rin=@(x) (1-J0(k*a)^2-J1(k*a)^2)*I(x(1))^2./(PTE^2*ZTE*sin(k0(i0)*x(1)).^2).*sin(bTE*x(2)).^2;
    Xin=@(x) -(1-J0(k*a)^2-J1(k*a)^2)*I(x(1))^2./(PTE^2*ZTE*sin(k0(i0)*x(1)).^2).*(sin(2*bTE*x(2))/2);

    % find length (l) and height (h) as z=[l,h] by matching to a 50 Ohm impedance 
    x0=fsolve(@(x) [Rin(x)-50 Xin(x)],[a/2 lambdag/10]);

    % calculate input impedance for all frequencies
    Zin=zeros(1,length(f0));
    for i=1:1:length(f0)
        ZTE=(Z0*k0(i))/sqrt(k0(i)^2-k^2);
        RTE=sqrt(pi/2)*J1(k*a)*sqrt(k^2*a^2-1);
        bTE=sqrt(k0(i)^2-k^2);
        PTE=2/(ZTE*RTE^2);
        I=integral(@(r) J1(k*r)./r.*sin(k0(i)*(r-a+x0(1))),a-x0(1),a);
        Rin=(1-J0(k*a)^2-J1(k*a)^2)*I^2/(PTE^2*ZTE*sin(k0(i)*x0(1)).^2)*sin(bTE*x0(2))^2;
        Xin=-(1-J0(k*a)^2-J1(k*a)^2)*I^2/(PTE^2*ZTE*sin(k0(i)*x0(1))^2)*(sin(2*bTE*x0(2))/2);
        Zin(i)=Rin+1i*Xin;
    end

    % 2nd method (Lee, 1994) 
    
    % ZTE=(Z0*k0(i))/sqrt(k0(i)^2-k(1,1)^2);
    % RTE=sqrt(pi/2)*J1(k(1,1)*a)*sqrt(k(1,1)^2*a^2-1);
    % bTE=sqrt(k0(i)^2-k(1,1)^2);
    % PTE=2/(ZTE*RTE^2);
    % I=@(h) integral(@(r) J1(k(1,1)*r)./r.*sin(k0(i)*(r-a+h)),a-h,a);

    w=zeros(1,5); % wronskian integrals
    w(1)=integral(@(r) r.*J1(k*r).^2,0,a);
    b1=sqrt(k0(i0)^2-k^2); % propagation constant of TE11 mode
    
    % real and imaginary parts of impedance as calculated by Lee's method
    R1=@(x) 2*pi*f0(i0)*10^9*mu0*sin(b1*x(2)).^2./(pi*w(1)*b1*k^2*sin(k0(i0)*x(1)).^2)*integral(@(r) J1(k*(a-r/k0(i0))).*sin(k0(i0)*x(1)-r)./(k0(i0)*a-r),0,k0(i0)*x(1))^2;
    %Rin=@(x) 2*pi*(1-J0(k(1,1)*a)^2-J1(k(1,1)*a)^2)*I(x(1))^2./(PTE^2*ZTE.*sin(k0(i)*x(1)).^2).*sin(bTE*x(2)).^2;
    X=@(x) 2*pi*f0(i0)*10^9*mu0*sin(2*b1*x(2))./(2*pi*w(1)*b1*k^2*sin(k0(i0)*x(1)).^2)*integral(@(r) J1(k*(a-r/k0(i0))).*sin(k0(i0)*x(1)-r)./(k0(i0)*a-r),0,k0(i0)*x(1))^2;
%     for n=2:1:5
%             w(n)=integral(@(r) r.*J1(k(1,n)*r).^2,0,a);
%             bn=sqrt(k0(i)^2-k(1,n)^2);
%             X=@(x) X(x)-16*1i*pi^2*f0(i)*10^9*mu0./(bn*k(1,n)^2*w(n)*sin(k0(i)*x(1)).^2)*integral(@(r) J1(k(1,n)*(a-r/k0(i))).*sin(k0(i)*x(1)-r)./(k0(i)*a-r),0,k0(i)*x(1))^2;
%     end
    
    % find length (l) and height (h) as z=[l,h] by matching to a 50 Ohm impedance 
    x1=fsolve(@(x) [R1(x)-50 X(x)],[a/2 lambda0(i0)/5]);

    % calculate input impedance for all frequencies
    Z=zeros(1,length(f0));
    for i=1:1:length(f0)
    %     ZTE=(Z0*k0(i))/sqrt(k0(i)^2-k(1,1)^2);
    %     RTE=sqrt(pi/2)*J1(k(1,1)*a)*sqrt(k(1,1)^2*a^2-1);
    %     bTE=sqrt(k0(i)^2-k(1,1)^2);
    %     PTE=2/(ZTE*RTE^2);
    %     I=integral(@(r) J1(k(1,1)*r)./r.*sin(k0(i)*(r-a+x1(1))),a-x1(1),a);
    %     
    %     Rin=2*pi*(1-J0(k(1,1)*a)^2-J1(k(1,1)*a)^2)*I^2/(PTE^2*ZTE*sin(k0(i)*x1(1)).^2)*sin(bTE*x1(2))^2;
        b1=sqrt(k0(i)^2-k^2);
        R1=2*pi*f0(i)*10^9*mu0*sin(b1*x1(2)).^2./(pi*w(1)*b1*k^2*sin(k0(i)*x1(1)).^2)*integral(@(r) J1(k*(a-r/k0(i))).*sin(k0(i)*x1(1)-r)./(k0(i)*a-r),0,k0(i)*x1(1))^2;
        X=2*pi*f0(i)*10^9*mu0*sin(2*b1*x1(2))./(2*pi*w(1)*b1*k^2*sin(k0(i)*x1(1)).^2)*integral(@(r) J1(k*(a-r/k0(i))).*sin(k0(i)*x1(1)-r)./(k0(i)*a-r),0,k0(i)*x1(1))^2;
%         for n=2:1:5
%             bn=sqrt(k0(i)^2-k(1,n)^2);
%             X=X-16*1i*pi^2*f0(i)*10^9*mu0./(bn*k(1,n)^2*w(n)*sin(k0(i)*x1(1)).^2)*integral(@(r) J1(k(1,n)*(a-r/k0(i))).*sin(k0(i)*x1(1)-r)./(k0(i)*a-r),0,k0(i)*x1(1))^2;
%         end 
        Z(i)=R1+1i*X;
    end
    
    % reflection coefficient
    Refpr=(Z-50)./(Z+50);
    
end



